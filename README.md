###Para buildar a aplicação e executar os testes:

`mvnw clean install`

Os testes foram implementados utilizando Cucumber e Junit. O cenário de teste está no arquivo `src/test/resources/UnitTests.feature`

###Para executar a aplicação:

`mvnw exec:java`


###Resultado:

URL                            | Qtde 
:------------------------------|:-----:
https://eagerhaystack.com      | 750 
https://surrealostrich.com.br  | 734 
https://grimpottery.net.br     | 732 

Status                            | Qtde 
:---|:-----:
200 | 1417 
201 | 1402 
400 | 1440 
500 | 1428 
204 | 1388 
404 | 1474 
503 | 1451 
