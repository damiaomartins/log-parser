package com.dm;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;


public class Parser {

    private final Path path;

    public Parser(Path path) {
        this.path = path;
    }

    public Map<String, Map<String, Long>> count(List<String> fields) throws IOException {
        try (Stream<String> lines = Files.lines(path, Charset.forName("ISO-8859-1"))) {

            Map<String, Map<String, Long>> result = new HashMap<>();
            fields.forEach(field -> result.put(field, new HashMap<>()));

            lines.forEach(line -> {
                String[] entries = line.split("\\s(?![^\\(\\[]*[\\)\\]])");
                for (String entry : entries) {
                    String[] keyValue = entry.split("=");
                    if (fields.contains(keyValue[0])) {
                        putEntry(result, keyValue);
                    }
                }
            });

            return result;
        }
    }

    private void putEntry(Map<String, Map<String, Long>> map, String[] keyValue) {
        String value = keyValue[1].replace("\"", "");
        map.get(keyValue[0]).compute(value, (k, v) -> v == null ? 1L : ++v);
    }
}
