package com.dm;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Consumer;

public class LogParserApplication {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("logs/log.txt");

        Parser parser = new Parser(path);

        Consumer<Map.Entry<String, Long>> printResult = s -> System.out.println(String.format("| %32s | %10s |", s.getKey(), s.getValue()));

        Map<String, Map<String, Long>> count = parser.count(Arrays.asList("request_to", "response_status"));

        System.out.println(String.format("\n| %32s | %10s |", "URL", "Qtde"));
        count.get("request_to")
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                .limit(3)
                .forEach(printResult);

        System.out.println(String.format("\n| %32s | %10s |", "Status", "Qtde"));
        count.get("response_status")
                .entrySet()
                .forEach(printResult);
    }


}