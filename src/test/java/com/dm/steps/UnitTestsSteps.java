package com.dm.steps;

import com.dm.Parser;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class UnitTestsSteps {

    private Parser parser;

    private Map<String, Map<String, Long>> result;

    private TemporaryFolder temporaryFolder;

    @Before
    public void before() throws IOException {
        temporaryFolder = new TemporaryFolder();
        temporaryFolder.create();
    }

    @After
    public void after() {
        temporaryFolder.delete();
    }


    @Dado("^que exista o arquivo de log com as seguintes linhas:$")
    public void queExistaOArquivoDeLogComAsSeguintesLinhas(List<String> logs) throws Throwable {
        File file = temporaryFolder.newFile("log.txt");
        Path logPath = file.toPath();
        Files.write(logPath, logs);
        parser = new Parser(logPath);
    }


    @Quando("^o job for executado para contagem dos campos:$")
    public void oJobForExecutadoParaContagemDe(List<String> fields) throws Throwable {
        result = parser.count(fields);
    }

    @Entao("^a contagem de \"([^\"]*)\" será:$")
    public void aContagemDeSerá(String field, Map<String, Long> expected) throws Throwable {
        Map<String, Long> actual = result.get(field);
        Assert.assertEquals(expected.size(), actual.size());
        expected.forEach((key, value) -> Assert.assertEquals(value, actual.get(key)));
    }
}
