package com.dm;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:UnitTests.feature",
        glue = "com.dm.steps",
        tags = "~@ignore",
        strict = true
)
public class LogParserApplicationTest {

}
