#language: pt
Funcionalidade: Contar ocorrencias no access_log

  Cenario: Contar ocorrencias de request_to e response_status
    Dado que exista o arquivo de log com as seguintes linhas:
      | level=info response_body="" response_status="204"                                          |
      | level=info response_body="" request_to="https://woodenoyster.com.br" response_status="404" |
      | level=info response_body="" request_to="https://grotesquemoon.de" response_status="200"    |
      | level=info response_body="" request_to="https://grotesquemoon.de"                          |
      | level=info response_body="" request_to="https://grimpottery.net.br" response_status="400"  |
      | level=info response_body="" request_to="https://grimpottery.net.br" response_status="200"  |
    Quando o job for executado para contagem dos campos:
      | request_to      |
      | response_status |
    Entao a contagem de "request_to" será:
      | https://grimpottery.net.br  | 2 |
      | https://grotesquemoon.de    | 2 |
      | https://woodenoyster.com.br | 1 |
    E a contagem de "response_status" será:
      | 200 | 2 |
      | 204 | 1 |
      | 400 | 1 |
      | 404 | 1 |